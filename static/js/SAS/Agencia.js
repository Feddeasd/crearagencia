var SAS_URL = "";

function getSASURL() {
    $.ajax({
        url: "/getSASURL",
        type: "GET",
        headers: {
            "Content-Type": "application/json"
        },
        timeout: 100000,
        success: function (response) {
            SAS_URL = response.SAS_URL;
        },
        error: function (x, e) {
            $('.progress').hide();
            cb({ "error": true, e });
        }
    });
}

function crearAgencia(Ciudad, CodigoPais, Piso, Barrio, Numero, OficinaODepartamento, CodigoEstado, Calle, CodigoTipoCalle, CEP, Email, PrimerNombre, NumeroDocumento, TipoDocumento,
    apellido, nombre, telefono, SUSEP, Segmento, cb) {
    var request = getCrearAgenciaRequest(Ciudad, CodigoPais, Piso, Barrio, Numero, OficinaODepartamento, CodigoEstado, Calle, CodigoTipoCalle, CEP, Email, PrimerNombre,
        NumeroDocumento, TipoDocumento, apellido, nombre, telefono, SUSEP, Segmento
    );

    $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000,
        success: function (response) {
            cb({ "error": false, response });
        },
        error: function (x, e) {
            $('.progress').hide();
            cb({ "error": true, e });
        }
    });
}

function getCrearAgenciaRequest(Ciudad, CodigoPais, Piso, Barrio, Numero, OficinaODepartamento, CodigoEstado, Calle, CodigoTipoCalle, CEP, Email, PrimerNombre, NumeroDocumento, TipoDocumento,
    apellido, nombre, telefono, SUSEP, Segmento) {
    return {
        "SessionID": "",
        "screenIdentification": "SASVJ0073",
        "Parameters": [
            {
                "parametername": "Address",
                "parameterlist": [
                    {
                        "parametername": "City",
                        "parametervalue": Ciudad
                    },
                    {
                        "parametername": "CountryCode",
                        "parametervalue": CodigoPais
                    },
                    {
                        "parametername": "Floor",
                        "parametervalue": Piso
                    },
                    {
                        "parametername": "Neighborhood",
                        "parametervalue": Barrio
                    },
                    {
                        "parametername": "Number",
                        "parametervalue": Numero
                    },
                    {
                        "parametername": "OfficeOrAppartment",
                        "parametervalue": OficinaODepartamento
                    },
                    {
                        "parametername": "StateCode",
                        "parametervalue": CodigoEstado
                    },
                    {
                        "parametername": "Street",
                        "parametervalue": Calle
                    },
                    {
                        "parametername": "StreetTypeCode",
                        "parametervalue": CodigoTipoCalle
                    },
                    {
                        "parametername": "ZipCode",
                        "parametervalue": CEP
                    }
                ]
            },
            {
                "parametername": "Email",
                "parametervalue": Email
            },
            {
                "parametername": "FirstName",
                "parametervalue": PrimerNombre
            },
            {
                "parametername": "IdentityDocumentNumber",
                "parametervalue": NumeroDocumento
            },
            {
                "parametername": "IdentityDocumentType",
                "parametervalue": TipoDocumento
            },
            {
                "parametername": "LastName",
                "parametervalue": apellido
            },
            {
                "parametername": "Name",
                "parametervalue": nombre
            },
            {
                "parametername": "Phone",
                "parametervalue": telefono
            },
            {
                "parametername": "SUSEP",
                "parametervalue": SUSEP
            },
            {
                "parametername": "Segment",
                "parametervalue": Segmento
            }
        ]
    }
}

function updateAgencia(ambiente, idAgencia, MotivoLHabilitado, MotivoEHabilitado, MotivoIHabilitado, MotivoMHabilitado, Calle, Numero, Piso, DeptoOficina, Ciudad,
    CodigoProvincia, CP, CodigoPais, CodigoTipoCalle, Barrio, EmailNuevo, TelefonoNuevo, RazonSocial, CNPJ, FechaBaja, SUSEP, UsuarioModificacion, cb) {
    var request = getUpdateAgenciaRequest(ambiente, idAgencia, MotivoLHabilitado, MotivoEHabilitado, MotivoIHabilitado, MotivoMHabilitado, Calle, Numero, Piso, DeptoOficina, Ciudad,
        CodigoProvincia, CP, CodigoPais, CodigoTipoCalle, Barrio, EmailNuevo, TelefonoNuevo, RazonSocial, CNPJ, FechaBaja, SUSEP, UsuarioModificacion);

    $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000,
        success: function (response) {
            cb({ "error": false, response });
        },
        error: function (x, e) {
            $('.progress').hide();
            cb({ "error": true, e });
        }
    });
}

function getUpdateAgenciaRequest(ambiente, idAgencia, MotivoLHabilitado, MotivoEHabilitado, MotivoIHabilitado, MotivoMHabilitado, Calle, Numero, Piso, DeptoOficina, Ciudad,
    CodigoProvincia, CP, CodigoPais, CodigoTipoCalle, Barrio, EmailNuevo, TelefonoNuevo, RazonSocial, CNPJ, FechaBaja, SUSEP, UsuarioModificacion) {
    console.log(MotivoLHabilitado)
    return {
        "screenIdentification": ambiente == 'OS' && "SASVJ0084" || "SASVJ0097",
        "Parameters": [
            { "parametername": "idAgencia", "parametervalue": idAgencia },
            { "parametername": "MotivoLHabilitado", "parametervalue": MotivoLHabilitado },
            { "parametername": "MotivoEHabilitado", "parametervalue": MotivoEHabilitado },
            { "parametername": "MotivoIHabilitado", "parametervalue": MotivoIHabilitado },
            { "parametername": "MotivoMHabilitado", "parametervalue": MotivoMHabilitado },
            { "parametername": "Calle", "parametervalue": Calle },
            { "parametername": "Numero", "parametervalue": Numero },
            { "parametername": "Piso", "parametervalue": Piso },
            { "parametername": "DeptoOficina", "parametervalue": DeptoOficina },
            { "parametername": "Ciudad", "parametervalue": Ciudad },
            { "parametername": "CodigoProvincia", "parametervalue": CodigoProvincia },
            { "parametername": "CP", "parametervalue": CP },
            { "parametername": "CodigoPais", "parametervalue": CodigoPais },
            { "parametername": "CodigoTipoCalle", "parametervalue": CodigoTipoCalle },
            { "parametername": "Barrio", "parametervalue": Barrio },
            { "parametername": "EmailNuevo", "parametervalue": EmailNuevo },
            { "parametername": "TelefonoNuevo", "parametervalue": TelefonoNuevo },
            { "parametername": "RazonSocial", "parametervalue": RazonSocial },
            { "parametername": "CNPJ", "parametervalue": CNPJ },
            { "parametername": "FechaBaja", "parametervalue": FechaBaja },
            { "parametername": "SUSEP", "parametervalue": SUSEP },
            { "parametername": "UsuarioModificacion", "parametervalue": UsuarioModificacion }
        ]
    }
}

function eliminarAgencia(ambiente, idAgencia, cb) {

    var request = getDeleteAgencyRequest(ambiente, idAgencia);

    $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000,
        success: function (response) {
            cb({ "error": false, response });
        },
        error: function (x, e) {
            $('.progress').hide();
            cb({ "error": true, e });
        }
    });
}


function getDeleteAgencyRequest(ambiente, idAgencia) {
    return {
        "screenIdentification": ambiente == 'OS' && 'SASVJ0078' || 'SASVJ0079',
        "Parameters": [
            { "parametername": "IdAgencia", "parametervalue": idAgencia }
        ]
    };
}

function listarAgencias(ambiente, cb) {
    var request = getListAgencyRequest(ambiente);

    $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000,
        success: function (response) {
            cb({ "error": false, response });
        },
        error: function (x, e) {
            $('.progress').hide();
            cb({ "error": true, e });
        }
    });
}

function getListAgencyRequest(ambiente) {
    return {
        "screenIdentification": ambiente == 'OS' && 'SASVJ0076' || 'SASVJ0077'
    };
}

function getDetalleAgencia(ambiente, IdAgencia) {
    var request = getDetalleAgenciaRequest(ambiente, IdAgencia);

    return $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000
    });
}

function getDetalleAgenciaRequest(ambiente, IdAgencia) {
    return {
        "screenIdentification": ambiente == 'OS' && 'SASVJ0082' || 'SASVJ0083',
        "Parameters": [
            { "parametername": "IdAgencia", "parametervalue": IdAgencia }
        ]
    };
}

function getIdAgenciaByCNPJ(ambiente, CNPJ, cb) {
    var request = getIdAgenciaByCNPJRequest(ambiente, CNPJ);

    return $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000
    });
}

function getIdAgenciaByCNPJRequest(ambiente, CNPJ) {
    return { "screenIdentification": ambiente == "OS" && "SASVJ0089" || "SASVJ0094", "Parameters": [{ "parametername": "CNPJ", "parametervalue": CNPJ }] }
}

function cargarTodosLosProductos(ambiente) {

    var data = getCargarTodosLosProductosRequest(ambiente)
    return $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data),
        timeout: 100000
    });
}

function getCargarTodosLosProductosRequest(ambiente) {
    return {
        "screenIdentification": ambiente == 'OS' && "SASVJ0071" || "SASVJ0072"
    }
}

function desvincularProductos(ambiente, idAgencia) {
    var data = getDesvincularProductosRequest(ambiente, idAgencia)
    return $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data),
        timeout: 100000
    });
}

function getDesvincularProductosRequest(ambiente, idAgencia) {
    return { "screenIdentification": ambiente == "OS" && "SASVJ0093" || "SASVJ0095", "Parameters": [{ "parametername": "idAgencia", "parametervalue": idAgencia }] }
}

function vincularProducto(ambiente, codProducto, idAgencia) {
    var request = getVincularProductoRequest(ambiente, codProducto, idAgencia);

    return $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000
    });
}

function getVincularProductoRequest(ambiente, codProducto, idAgencia) {
    return {
        "screenIdentification": ambiente == "OS" && "SASVJ0092" || "SASVI0096",
        "Parameters": [
            { "parametername": "idAgencia", "parametervalue": idAgencia },
            { "parametername": "CodigoProducto", "parametervalue": codProducto }
        ]
    }
}

function vincularPadronBasicoProductos(ambiente, IdAgencia, cb) {
    var request = getVincularPadronBasicoProductosRequest(ambiente, IdAgencia);

    $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000,
        success: function (response) {
            cb({ "error": false, response });
        },
        error: function (x, e) {
            $('.progress').hide();
            cb({ "error": true, e });
        }
    });
}

function getVincularPadronBasicoProductosRequest(ambiente, idAgencia) {
    return { "screenIdentification": ambiente == 'OS' && "SAS0030" || 'SAS0029', "Parameters": [{ "parametername": "idAgencia", "parametervalue": idAgencia }] }
}

function obtenerVendedoresByIdAgencia(ambiente, idAgencia, cb) {
    //console.log("entro");
    var data = getCargarTodosLosVendedoresRequest(ambiente, idAgencia)
    return $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data),
        timeout: 100000,
        success: function (response) {
            cb({ "error": false, response });
        },
        error: function (x, e) {
            cb({ "error": true, e });
        }
    });
}

function getCargarTodosLosVendedoresRequest(ambiente, IdAgencia) {
    return {
        "screenIdentification": ambiente == 'OS' && "SASVJ0074" || "SASVJ0075",
        "Parameters": [
            { "parametername": "IdAgencia", "parametervalue": IdAgencia }
        ]
    }
}

function do_datatable(data) {

    $('#example').DataTable({
        order: [[1, 'asc']],
        data: data,
        deferRender: true,
        columns: [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            { data: "Email" },
            { data: "Nombre" },
            { data: "Apellido" },
            { data: "FechaNacimiento" },
            { data: "Numero" },
            { data: "FechaBaja" }
        ],
        select: {
            style: 'os',
            selector: 'td:first-child'
        }
    });

    var table = $('#example').DataTable()

    table.on('select', function (e, dt, type, indexes) {
        $("#EditarVendedor").prop("disabled", false)
        $("#EliminarVendedor").prop("disabled", false)
    });

    table.on('deselect', function (e, dt, type, indexes) {
        console.log(dt.data())
        $("#EditarVendedor").prop("disabled", true)
        $("#EliminarVendedor").prop("disabled", true)
    });

    $('#NovoVendedor').click(function (ev) {
        ev.preventDefault()
        ModoVendedor = 'A'
    })

    $('#btnModalSalvar').click(function (ev) {
        ev.preventDefault()
        console.log(ModoVendedor)
        var Email = $("#inpModalEmail").val()
        var Nombre = $("#inpModalNome").val()
        var Apellido = $("#inpModalSobrenome").val()
        var FechaNacimiento = $("#inpModalDtNas").val()
        var Numero = $("#inpModalCPF").val()

        var CPFValido = TestaCPF(Numero)
        if(Numero.length == 0 || !CPFValido){
            alert("CPF inválido")
            return
        }
        if(Email.length == 0 || !Email){
            alert("Email inválido")
            return
        }
        if(Nombre.length == 0 || !Nombre){
            alert("Nome inválido")
            return
        }
        if(Apellido.length == 0 || !Apellido ){
            alert("Sobrenome inválido")
            return
        }
        if(FechaNacimiento.length == 0 || !FechaNacimiento){
            alert("Data de nascimento inválida")
            return
        }

        if (ModoVendedor == 'A') {

            console.log("Vendedores: ", vendedores)

            vendedores.push({ Email, Nombre, Apellido, FechaNacimiento, Numero, FechaBaja: "", IdVendedor: "", Sexo: "M", IdUser: "" })

            table
                .row
                .add({ Email, Nombre, Apellido, FechaNacimiento, Numero, FechaBaja: "", IdVendedor: "", Sexo: "M", IdUser: "" })
                .draw()

            $("#inpModalEmail").val("")
            $("#inpModalNome").val("")
            $("#inpModalSobrenome").val("")
            $("#inpModalDtNas").val("")
            $("#inpModalCPF").val("")
        }
        else {
            var filaSeleccionada = table.row({ selected: true })
            var datosFila = filaSeleccionada.data()

            vendedores.push({ Email, Nombre, Apellido, FechaNacimiento, Numero, FechaBaja: datosFila.FechaBaja, IdVendedor: datosFila.IdVendedor, Sexo: datosFila.Sexo, IdUser: datosFila.IdUser })
            table
                .row(filaSeleccionada)
                .data({ Email, Nombre, Apellido, FechaNacimiento, Numero, FechaBaja: datosFila.FechaBaja, IdVendedor: datosFila.IdVendedor, Sexo: datosFila.Sexo, IdUser: datosFila.IdUser })
                .draw();

            $("#inpModalEmail").val("")
            $("#inpModalNome").val("")
            $("#inpModalSobrenome").val("")
            $("#inpModalDtNas").val("")
            $("#inpModalCPF").val("")
        }

        $('#modalSubscriptionForm').modal('hide')

    })

    $('#EditarVendedor').click(function (ev) {

        ev.preventDefault()
        ModoVendedor = 'M'
        var seleccionado = table.row({ selected: true }).data()
        console.log(seleccionado.FechaNacimiento.substring(0, 9))
        $("#inpModalEmail").val(seleccionado.Email)
        $("#inpModalNome").val(seleccionado.Nombre)
        $("#inpModalSobrenome").val(seleccionado.Apellido)
        $("#inpModalDtNas").val(seleccionado.FechaNacimiento.substring(0, 10))
        $("#inpModalCPF").val(seleccionado.Numero)
    })

    $('#EliminarVendedor').click(function (ev) {
        ev.preventDefault()
        var filaSelec = table.row({ selected: true })
        var dato = filaSelec.data()
        console.log(filaSelec)
        var conf = confirm("Deseja desativar o vendedor " + dato.Email + "?")
        if (conf == true) {
            if (dato.IdVendedor) {
                eliminarVendedor(AMBIENTE, dato.IdVendedor, function (resultElim) {
                    if (resultElim.error == false) {
                        alert("Vendedor desativado com sucesso")
                        window.location.reload()
                    }
                })
            }

            filaSelec.remove().draw();
        }
    })


}

function validarCNPJ(cnpj) {
 
    cnpj = cnpj.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj === "00000000000000" || 
        cnpj === "11111111111111" || 
        cnpj === "22222222222222" || 
        cnpj === "33333333333333" || 
        cnpj === "44444444444444" || 
        cnpj === "55555555555555" || 
        cnpj === "66666666666666" || 
        cnpj === "77777777777777" || 
        cnpj === "88888888888888" || 
        cnpj === "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
    
}

function TestaCPF(strCPF) {
    var Soma;
    var Resto;
    Soma = 0;
    
    strCPF = strCPF.split('\.').join('').split('-').join('')
    if (strCPF == "00000000000") return false;

    for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10))) return false;

    Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11))) return false;
    return true;
}

function UpdateVendedor(ambiente, CountryCode, IdAgencia, Login, Code, NumeroDoc, DateOfBirth, Gender, FirstName, LastName, DNNUSerID, Email, CodArea, NumeroTel, TypeTel, SalesManID, SalesManUserName, Supervisor) {

    var request = getUpdateVendedorRequest(ambiente, CountryCode, IdAgencia, Login, Code, NumeroDoc, DateOfBirth, Gender, FirstName, LastName, DNNUSerID, Email, CodArea, NumeroTel, TypeTel, SalesManID, SalesManUserName, Supervisor);
    return $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000
    });
}

function getUpdateVendedorRequest(ambiente, CountryCode, IdAgencia, Login, Code, NumeroDoc, DateOfBirth, Gender, FirstName, LastName, DNNUSerID, Email, CodArea, NumeroTel, TypeTel, SalesManID, SalesManUserName, Supervisor) {
    return {
        "screenIdentification": ambiente == "OS" && "SASVJ0085" || "SASVJ0086",
        "Parameters": [
            {
                "parametername": "CountryCode",
                "parametervalue": CountryCode
            },
            {
                "parametername": "IdAgencia",
                "parametervalue": IdAgencia
            },
            {
                "parametername": "Login",
                "parametervalue": Login
            },
            {
                "parametername": "Seller",
                "parameterlist": [
                    {
                        "parametername": "IdentityDocuments",
                        "parameterlist": [
                            {
                                "parametername": "IdentityDocument",
                                "parameterlist": [
                                    {
                                        "parametername": "Code",
                                        "parametervalue": Code
                                    },
                                    {
                                        "parametername": "Number",
                                        "parametervalue": NumeroDoc
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "parametername": "DateOfBirth",
                        "parametervalue": DateOfBirth
                    },
                    {
                        "parametername": "FirstName",
                        "parametervalue": FirstName
                    },
                    {
                        "parametername": "Gender",
                        "parametervalue": Gender
                    },
                    {
                        "parametername": "LastName",
                        "parametervalue": LastName
                    },
                    {
                        "parametername": "DNNUSerID",
                        "parametervalue": DNNUSerID
                    },
                    {
                        "parametername": "EmailAddress",
                        "parameterlist": [
                            {
                                "parametername": "Email",
                                "parametervalue": Email
                            }
                        ]
                    },
                    {
                        "parametername": "Phones",
                        "parameterlist": [
                            {
                                "parametername": "Phone",
                                "parameterlist": [
                                    {
                                        "parametername": "AreaCode",
                                        "parametervalue": CodArea
                                    },
                                    {
                                        "parametername": "Number",
                                        "parametervalue": NumeroTel
                                    },
                                    {
                                        "parametername": "Type",
                                        "parametervalue": TypeTel
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "parametername": "SalesManID",
                        "parametervalue": SalesManID
                    },
                    {
                        "parametername": "SalesManUserName",
                        "parametervalue": SalesManUserName
                    },
                    {
                        "parametername": "Supervisor",
                        "parametervalue": Supervisor
                    }
                ]
            }
        ]
    }
}

function eliminarVendedor(ambiente, IdVendedor, cb) {

    var request = getDeleteVendedorRequest(ambiente, IdVendedor, idAgencia);

    $.ajax({
        url: SAS_URLHOMO,
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(request),
        timeout: 100000,
        success: function (response) {
            cb({ "error": false, response });
        },
        error: function (x, e) {
            $('.progress').hide();
            cb({ "error": true, e });
        }
    });
}


function getDeleteVendedorRequest(ambiente, IdVendedor) {
    return {
        "screenIdentification": ambiente == 'OS' && 'SASVJ0087' || 'SASVJ0088',
        "Parameters": [
            { "parametername": "SellerId", "parametervalue": IdVendedor }
        ]
    };
}
