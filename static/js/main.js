

(function ($) {
    "use strict";
    var SAS_URLHOMO ="http://srvseg-homolog.omintseguroviagem.com.br:9600/api/SASData/Get_V2";
    var PORTHOMO ="9601";


    var SAS_URLPROD ="http://omintseguroviagem.com.br:9600/api/SASData/Get_V2";
    var PORTPROD ="9601";


    var SAS_URLLOCAL ="http://localhost:9600/api/SASData/Get_V2";
    var PORTLOCAL ="9601";


    var AMBIENTE = '1';

    $( '#ambiente' ).change(function(ev) {
      ev.preventDefault();


      AMBIENTE = ev.target.value;

      $("#ambiente").val(AMBIENTE);

      console.log(AMBIENTE);

      if(ev.target.value ='1')
      {
        $("#ambiente").val(AMBIENTE);

      var data = {
      "screenIdentification":"SASVJ0071",
      "Parameters":[]}
      $.ajax({
          url: SAS_URLHOMO,
          type: "POST",
          headers: {
          "Content-Type":"application/json"
          },
          data: JSON.stringify(data),
          timeout: 100000,
          success: function(response) {
            $('#searchResults').show();
            var productos =  response.ResponseJSONData;
            var table = $("#dt-basic-checkbox");
            var trHTML = '';
            $.each(productos, function (index,item) {
                  $('#dt-basic-checkbox').DataTable().row.add( [
                    "",
              item.CodigoProducto   ,
              item.DenominacionProducto
          ] ).draw( false );
                   //.append("<tr><td>" + "</td><td>"+item.CodigoProducto+"</td>   <td>"+item.DenominacionProducto+"</td></tr>");
            });

          },
          error: function(x, e) {
              $('.progress').hide();

          },
          failure: function(response) {
              $('.progress').hide();
              alert('Error al realizar la operación')
          }
      });
  }
  else {

  var data = {
  "screenIdentification":"SASVJ0072",
  "Parameters":[]}
  $.ajax({
      url: SAS_URLHOMO,
      type: "POST",
      headers: {
      "Content-Type":"application/json"
      },
      data: JSON.stringify(data),
      timeout: 100000,
      success: function(response) {
        $('#searchResults').show();
        var productos =  response.ResponseJSONData;
        var table = $("#dt-basic-checkbox");
        var trHTML = '';
        $.each(productos, function (index,item) {
              $('#dt-basic-checkbox').DataTable().row.add( [
                "",
          item.CodigoProducto   ,
          item.DenominacionProducto
      ] ).draw( false );
               //.append("<tr><td>" + "</td><td>"+item.CodigoProducto+"</td>   <td>"+item.DenominacionProducto+"</td></tr>");
        });

      },
      error: function(x, e) {
          $('.progress').hide();

      },
      failure: function(response) {
          $('.progress').hide();
          alert('Error al realizar la operación')
      }
  });
}




  });
    /*==================================================================
    [ Validate after type ]*/
    $('.validate-input .input100').each(function(){
        $(this).on('blur', function(){
            if(validate(this) == false){
                showValidate(this);
            }
            else {
                $(this).parent().addClass('true-validate');
            }
        })
    })


    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
           $(this).parent().removeClass('true-validate');
        });
    });

     function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');

        $(thisAlert).append('<span class="btn-hide-validate">&#xf136;</span>')
        $('.btn-hide-validate').each(function(){
            $(this).on('click',function(){
               hideValidate(this);
            });
        });
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
        $(thisAlert).find('.btn-hide-validate').remove();
    }

    function getAgencias(){
        
    }



})(jQuery);
