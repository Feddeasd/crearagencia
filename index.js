const express = require('express');
const app = express()
const config = require('config')
const path = require('path')

app.use(express.static('static'));

app.get("/getSASURL", (req, res) => {
    res.send({"SAS_URL": config.get("SAS_URL")})
})

app.get("/", (req, res) => {
    res.sendFile(path.resolve(__dirname, 'static' , 'Pages', 'ConsultaAgencias.html'));
})

app.get("/agencia/:idAgencia", (req, res) => {
    res.sendFile(path.join(__dirname, 'static', 'Pages', 'CrearAgencia.html'));
})

app.listen(config.get("PORT"), () => {
    console.log("App en puerto " + config.get("PORT"))
})